Учебный проект для курса HTML+CSS


Полезные ссылки:

  справочник HTML\CSS: www.webref.ru/html, www.webref.ru/css 
  флексы, все свойства: https://codepen.io/enxaneta/full/adLPwv/
  статья про отступы между элементами: https://habr.com/post/340420/
  статьи про поток документа: http://softwaremaniacs.org/blog/2005/08/27/css-layout-flow/ , http://softwaremaniacs.org/blog/2005/09/05/css-layout-flow-margins/

  основные цвета: #F29008 - кнопки, фон
                #EEEEEE - основной фон 
                #525252 - цвет шрифта в футере

  генератор теней: https://www.cssmatic.com/box-shadow
  
  Препроцессоры: Потренироваться и посмотреть на скомпилированный CSS-код можно на сайтах https://sassmeister.com  – для Sass и на https://lesstester.com – для Less
  
  чтобы запустить автоматизацию препроцессора под Windows, надо скачать и установить Ноду с nodejs.org. Также желательно установить консоль Git (https://git-scm.com/downloads).
  Затем в проекте по правой кнопке мыши запускаем консоль и в ней прописываем команду: npm install - устанавливаем модули Ноды. 
  После того, как модули установились, можно запускать проект командой "npm start". Не забывайте, что надо предварительно скопировать и положить gulpfile.js и package.json в корневую папку проекта.